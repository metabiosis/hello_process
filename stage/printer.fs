\    This file is part of Hello Process.
\
\    Hello Process is free software: you can redistribute it and/or modify
\    it under the terms of the GNU General Public License as published by
\    the Free Software Foundation, either version 3 of the License, or
\    (at your option) any later version.
\
\    Hello Process is distributed in the hope that it will be useful,
\    but WITHOUT ANY WARRANTY; without even the implied warranty of
\    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\    GNU General Public License for more details.
\
\    You should have received a copy of the GNU General Public License
\    along with Hello Process.  If not, see <http://www.gnu.org/licenses/>.

\ Printer
\ This file contains all the words needed to control the printer from
\ the motherhip

\ utils
create str1 256 allot
create str2 256 allot
: S+  str2 place str1 place str2 count str1 +place str1 count ;

\ head control
: form-feed    s\" echo \"\f\" | lp -d oki -o raw" system ;
: new-line     s\" echo \"\n\" | lp -d oki -o raw" system ;

\ rendering
: hello-process
    s\" echo -e \"@59l\11Q\110!\101A theater of naive computation.\n\n!\131Hello Process!!\101 `stat -c %Y /tmp/fb/hello-process.fb`\n\nForty lines, Forty iterations of a file. Each line represents\none iteration. Every line consists of 128 blocks. Each block\ncan contain a small program of up to 1024 bytes. The blocks\nare executed one after the other. Each program can move, copy,\nswap or delete any block in the file.\" | lp -d oki -o raw"
    system
;

: send-printer
    >r >r s\" echo -e \"@59l\11Q\110!\101" r> r> S+
    s\" \"" S+
    S"  | lp -d oki -o raw" S+
    system
;

: draw-blocks  S" ./block-display.py | lp -d oki -o raw" system ;



\ `stat -c %Y hello-process.fb`
