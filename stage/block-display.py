#!/usr/bin/python
#
#
#   This file is part of Hello Process.
#
#   Hello Process is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Hello Process is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Hello Process.  If not, see <http://www.gnu.org/licenses/>.
#
#

import shutil

blockfb = "/tmp/fb/hello-process.fb"
blockcp = "/tmp/fb/hello-process-copy.fb"

shutil.copy(blockfb, blockcp)

file = open(blockcp, "r")
fb = file.read()

map = "@59\t*1\0\3"

for char in range(128):
    if char != " ":
        map += fb[ char * 1024 + 2] * 6
    else:
        map += "@" * 6

map += "@"

print map
