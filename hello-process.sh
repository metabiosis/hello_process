#!/bin/sh
# startup script
#
#
#   This file is part of Hello Process.
#
#   Hello Process is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Hello Process is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Hello Process.  If not, see <http://www.gnu.org/licenses/>.
#
#


HPDIR="/home/lintian/hello_process/stage"
FBHOST="/tmp/fb"


while true
do
    cd $HPDIR

    # make sure tmpfs is created
    if [ ! -d $FBHOST ]
    then
        mkdir $FBHOST
    fi

    if ! mountpoint -q $FBHOST
    then
        sudo mount -t tmpfs -o size=1M,nr_inodes=10k,mode=0777 tmpfs $FBHOST
    fi

    # flush the toilet
    cancel -a
    lprm -

    # hello process!
    gforth mothership.fs 30000
done

